import {expect, jest, test} from '@jest/globals';
import BinarySearchTree from "../src/binary-search-tree";

/*
        8
       / \
      3   10
     / \    \
    1   6    14
       / \   /
      4   7 13

*/
test('Should find 13 in given tree', () => {
    const bst1 = new BinarySearchTree();
    [8, 3, 10, 1, 6, 14, 4, 7, 13].forEach(value => bst1.append(value));
    // console.log(JSON.stringify(bst1, '', 4));

    const found = bst1.find(13);

    expect(found).not.toBeUndefined();
});