export default class BinarySearchTree {
    constructor() {
        this.root = null;
    }

    append(value) {
        if (this.root == null) {
            this.root = new Node(value);
            return;
        }

        this.root.append(value);
    }

    find(value) {
        return this.root.find(value);
    }

    print() {
        return;
    }
}

class Node {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }

    find(value) {
        if (this.value === value) {
            return this;
        }

        if (value < this.value) {
            if (value === this.left.value) {
                return this.left;
            }

            return this.left.find(value);
        }

        if (value > this.value) {
            if (value === this.right.value) {
                return this.right;
            }

            return this.right.find(value);
        }
    }

    append(value) {
        if (value < this.value) {
            if (this.left == null) {
                this.left = new Node(value);
                return;
            }

            this.left.append(value);
            return;
        }
        
        if (value > this.value) {
            if (this.right == null) {
                this.right = new Node(value);
                return;
            }
            
            this.right.append(value);
            return;
        }
    }
}